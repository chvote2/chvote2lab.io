---
layout: article
title: Demo
excerpt: Video demonstrating the usage of the CHVote applications to run a votation
tags: CHVote, evoting, opensource, demo, video, usage, votation
permalink: /demo/
image:
  feature: demo-banner.png
---

Watch the following video to take a tour on the whole process of preparing a votation, opening
the electronic voting channel, casting ballots, decrypting the ballot box, and generating and verifying
the results with the CHVote 2.0 system.

### Demo in French 
<iframe frameborder="0" width="480" height="360" src="https://vod.infomaniak.com/iframe.php?url=directiongnraledessystmes_1_vod/internet-42367/mp4-1171/chvotedemofr.mp4&player=7019&vod=3992&preloadImage=directiongnraledessystmes_1_vod/internet-42367/mp4-1171/chvotedemofr.jpg&wmode=direct" allowfullscreen></iframe>

Click the following link to download the video: [demonstration video](/videos/chvote-demo-fr.mp4)

### Demo in English
*Soon to be published.*
