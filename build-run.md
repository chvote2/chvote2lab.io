---
layout: article
title: Build & Run
excerpt: Build and run the CHVote system from the source code
tags: CHVote, evoting, opensource, AGPL, release, source code, git, gitlab, build, run, test, exercise
permalink: /build-run/
image:
  feature: build-run-banner.png
---

{% include toc.html %}

## 1. Prerequisites

### 1.1 Hardware

A minimum of 16 GB of RAM is needed to run the full stack of the CHVote system.

### 1.2 Development tools

To build and run the CHVote applications, the following tools have to be installed on your environment:

* Java Development Kit 11: [OpenJDK](https://jdk.java.net/11/) or [AdoptOpenJDK](https://adoptopenjdk.net/)
* Java Development Kit 8 (for the protocol core components): [OpenJDK](https://jdk.java.net/8/) or [AdoptOpenJDK](https://adoptopenjdk.net/)
* [Maven 3](https://maven.apache.org/)
* NodeJS 10 LTS and npm for frontend development: [nodejs](https://nodejs.org/en/download/)
* [Docker](https://www.docker.com/)
* [Docker compose](https://docs.docker.com/compose/install/)
* [Git](https://git-scm.com/)

Please read the relevant installation documentation of each of these components to setup them properly in your
own environment.

Set your JAVA_HOME environment variable to your JDK 11 installation directory.

### 1.3 Docker environment variables

On a Linux host add the following environment variables:

```
DOCKER_COMPOSE_LOCATION=<your docker-compose installation directory>
DOCKER_HOSTNAME=localhost
```

On a Windows host add the following environment variables
(adapt with the results from running the command `docker-machine env`):

```
DOCKER_LOCATION=<Docker Toolbox install directory>\docker.exe
DOCKER_TLS_VERIFY=1
DOCKER_MACHINE_NAME=default
DOCKER_HOSTNAME=192.168.99.100
DOCKER_HOST=tcp://192.168.99.100:2376
DOCKER_CERT_PATH=C:\Users\<your username>\.docker\machine\machines\default
DOCKER_COMPOSE_LOCATION=<Docker Toolbox install directory>\docker-compose.exe
COMPOSE_CONVERT_WINDOWS_PATHS=true
```

### 1.4 Required ports

The defaults ports used by the stack are the following and must be free:

|Port|Component|
|:----|:---------|
5672  |RabbitMQ AMQP Port
15672 |RabbitMQ Management Port
8090  |Bulletin Board
8190  |Bulletin Board (management)
5002  |Bulletin Board PostgreSQL Database
8091  |Control Component #0
5000  |Control Component #0 PostgreSQL Database
8092  |Control Component #1
5001  |Control Component #1 PostgreSQL Database
8093  |Protocol runner
8281  |Pact Backend (front api)
8282  |Pact Backend (b2b api)
8888  |Pact Backend (management api)
47646 |Pact Backend (Mock Server)
8283  |Pact Backend (Scheduler)
47645 |Pact H2 Database
4200  |Pact Frontend
9753  |BO Backend (front api)
9755  |BO Backend (management)
9754  |BO Mock Server
9756  |BO Mock Server (management)
9757  |BO H2 Database
4300  |BO Frontend
8080  |Vote Receiver Backend
57648 |Vote Receiver Mock Server
57645 |Vote Receiver H2 Database
4400  |Vote Receiver Frontend
8381  |VRUM (front api)
8382  |VRUM (b2b api)
8889  |VRUM (management api)
48646 |VRUM (Mock server)
48645 |VRUM H2 Database
4500  |VRUM Frontend

### 1.5 Setup dependencies

This build & run documentation provides the fastest way to build and start the components of CHVote.
It relies on downloading the required maven and npm dependencies from public repositories.

> As another way to get those dependencies locally, you can of course checkout the repositories of each dependency
and build and install them yourself.

The npm dependency is published directly to the npmjs.com repository ([https://www.npmjs.com/package/@chvote/chvote-common-theme](https://www.npmjs.com/package/@chvote/chvote-common-theme)),
so no specific configuration is needed here. 

However, to download the dependencies from the maven public repository, you need to tell maven to connect to our
public repository. The following xml snippet is an example of how to configure the maven settings.xml file
to use the repository:

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<settings xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd'
         xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
   <profiles>
       <profile>
           <repositories>
               <repository>
                   <snapshots>
                       <enabled>false</enabled>
                   </snapshots>
                   <id>bintray-chvote-etat-de-geneve-chvote-maven</id>
                   <name>bintray</name>
                   <url>https://dl.bintray.com/chvote-etat-de-geneve/chvote-maven</url>
               </repository>
           </repositories>
           <id>bintray</id>
       </profile>
   </profiles>
   <activeProfiles>
       <activeProfile>bintray</activeProfile>
   </activeProfiles>
</settings>  
```  

## 2. Clone the applications repositories

Clone the repositories unto your development device in a fresh workspace (its path will be referred as WORKSPACE_CHVOTE
in the following command lines): 

```
cd $WORKSPACE_CHVOTE
git clone {{ site.chvote-root-url }}/protocol-core/chvote-protocol.git
git clone {{ site.chvote-root-url }}/web-apps/chvote-pact.git
git clone {{ site.chvote-root-url }}/web-apps/chvote-bo.git
git clone {{ site.chvote-root-url }}/web-apps/chvote-receiver.git
git clone {{ site.chvote-root-url }}/web-apps/chvote-vrum.git
git clone {{ site.chvote-root-url }}/offline-apps/u-print.git
git clone {{ site.chvote-root-url }}/offline-apps/u-count.git
git clone {{ site.chvote-root-url }}/offline-apps/chvote-prover.git
```

## 3. Start the required middleware

The base image for the control components database must be built locally (chvote/cc-database). This
must be done by running the system tests of the protocol components:

```
cd $WORKSPACE_CHVOTE/chvote-protocol

JAVA_HOME=<your jdk 8 home> \
MB_HOST=$DOCKER_HOSTNAME \
MB_USERNAME=rabbitmq \
MB_PASSWORD=p4ssW0rd \
mvn -P system-tests verify
```

Please read https://gitlab.com/chvote2/protocol-core/chvote-protocol#oracle-ojdbc-driver to manage
the oracle jdbc driver dependency.

If the system tests succeed, you can then launch RabbitMQ and 
the 3 PostgreSQL databases for the control components and the bulletin board:

```
docker-compose -f system-tests/docker-compose.yml -p systemtests up
```

## 4. Start the Bulletin board

```
cd $WORKSPACE_CHVOTE/chvote-protocol/bulletin-board/bulletin-board-messaging

JAVA_HOME=<your jdk 8 home> \
SPRING_RABBITMQ_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=dev \
SECURITY_LEVEL=1 \
mvn spring-boot:run
```

## 5. Start the Control components

Two control components must be launched.

Run the Control Component #0:

```
cd $WORKSPACE_CHVOTE/chvote-protocol/control-component/control-component-messaging

JAVA_HOME=<your jdk 8 home> \
SPRING_RABBITMQ_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=dev_0 \
CC_INDEX=0 \
SECURITY_LEVEL=1 \
mvn spring-boot:run
```

> If you're using the Windows Docker toolbox, add `windows_0` to the `SPRING_PROFILES_ACTIVE` environment variable.


Run the Control Component #1:

```
cd $WORKSPACE_CHVOTE/chvote-protocol/control-component/control-component-messaging

JAVA_HOME=<your jdk 8 home> \
SPRING_RABBITMQ_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=dev_1 \
CC_INDEX=1 \
SECURITY_LEVEL=1 \
mvn spring-boot:run
```

> If you're using the Windows Docker toolbox, add `windows_1` to the `SPRING_PROFILES_ACTIVE` environment variable.

## 6. Test the protocol core components

Run a test with the protocol runner: 

```
cd $WORKSPACE_CHVOTE/chvote-protocol/protocol-runner

JAVA_HOME=<your jdk 8 home> \
SPRING_RABBITMQ_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=runner,voteclient \
SECURITY_LEVEL=1 \
SERVER_PORT=8093 \
mvn spring-boot:run -Drun.arguments=--host=$DOCKER_HOSTNAME,--port=5672,--username=rabbitmq,--password=p4ssW0rd,--nbControlComponents=2,--votersCount=10,--waitTimeout=1200000,--securityLevel=1,--electionSet=SIMPLE_SAMPLE
```

The tests is successful if a test report in markdown format have been generated in 
`$WORKSPACE_CHVOTE/chvote-protocol/protocol-runner/target`. You should see in the protocol runner 
log the following trace:
`Verification data written at $WORKSPACE_CHVOTE/chvote-protocol/protocol-runner/./target/TestReport_<date>.md`.


Once the report has been generated, you can stop the protocol runner (Crtl+C), as it keeps running
because it also runs a vote client that do not stop by itself (always listening).

## 7. Start the PACT web application

Build the PACT backend applications:

```
cd $WORKSPACE_CHVOTE/chvote-pact/pact-back
CHVOTE_OUTPUT_DIR=. mvn clean install
```

Run the PACT main backend application:

```
cd $WORKSPACE_CHVOTE/chvote-pact/pact-back/pact-back-rest

SPRING_PROFILES_ACTIVE=dev \
MB_HOST=$DOCKER_HOSTNAME \
MB_PORT=5672 \
MB_USERNAME=rabbitmq \
MB_PASSWORD=p4ssW0rd \
CHVOTE_OUTPUT_DIR=. \
SECURITY_LEVEL=1 \
CHVOTE_VR_IDENTIFICATION_URL=http://localhost:4400/{0}vote/{1}/{2}/{3}/identification \
CHVOTE_PACT_CONTEXT_URL=http://localhost:4200 \
mvn spring-boot:run
```

Run the PACT mock server:

```
cd $WORKSPACE_CHVOTE/chvote-pact/pact-back/pact-back-mock-server/pact-back-mock-server-rest
mvn spring-boot:run
```

Run the PACT scheduler (currently used to automatically start the generation of  the results 
after the voting period has ended) :

```
cd $WORKSPACE_CHVOTE/chvote-pact/pact-back/pact-back-scheduler
SERVER_PORT=8283 mvn spring-boot:run
```

Build and run the PACT frontend:

```
cd $WORKSPACE_CHVOTE/chvote-pact/pact-front/
npm install
npm start
```

if you encounter a `Cannot read property 'match' of undefined` error when running `npm install`, delete
the `package-lock.json` file before running `npm install` again.

To test the user interface, go to [http://localhost:4200](http://localhost:4200) and log in with user `user1` and 
password `password`.

To generate some test data, go to the mock server UI at [http://localhost:47646/mock/swagger-ui.html](http://localhost:47646/mock/swagger-ui.html):

- First, run `GET /database/reset`
- Then run `GET /operations/create`

Go back to the PACT UI, you should now see 2 `Demandes en cours`.

## 8. Start the BO web application

Build the BO backend:

```
cd $WORKSPACE_CHVOTE/chvote-bo/backend
mvn clean install
```

Run the BO backend application:

```
cd $WORKSPACE_CHVOTE/chvote-bo/backend/bo-backend-rest

SPRING_PROFILES_ACTIVE=development,with-local-pact \
PACT_VOTINGMATERIALBASELOCATION=$WORKSPACE_CHVOTE/chvote-pact/pact-back/pact-back-rest/target/classes/ \
mvn spring-boot:run
```

> In the previous command line, don't forget to replace `$WORKSPACE_CHVOTE` with the actual value. 

Run the BO mock server:

```
cd $WORKSPACE_CHVOTE/chvote-bo/backend/bo-backend-mock-server

SPRING_PROFILES_ACTIVE=development \
mvn spring-boot:run
```

Build and run the BO frontend:

```
cd $WORKSPACE_CHVOTE/chvote-bo/frontend/
npm install -g @angular/cli
npm install
npm start
```

To test the user interface, go to [http://localhost:4300](http://localhost:4300) and log in with user `ge1` and 
password `pass`.

To generate some test data, go to the mock server UI at [http://localhost:9754/mock/swagger-ui.html](http://localhost:9754/mock/swagger-ui.html):

- First, run `GET /database/reset`
- Then run `GET /database/operation/create/{shortName}/{longName}`. In the options,
fill the shortName and the longName, and choose the following options: `ADD_MILESTONE`,
`ADD_DOMAIN_INFLUENCE` and `ADD_REPOSITORY_VOTATION`. 

Go back to the BO UI, you should now see your newly created operation.

## 9. Start the Vote Receiver web application

Build the Vote Receiver backend:

```
cd $WORKSPACE_CHVOTE/chvote-receiver/backend
mvn clean install
```

Run the Vote Receiver backend application:

```
cd $WORKSPACE_CHVOTE/chvote-receiver/backend/vr-backend-rest

MB_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=dev,with-local-pact \
SECURITY_LEVEL=1 \
mvn spring-boot:run
```

Run the Vote Receiver mock server:

```
cd $WORKSPACE_CHVOTE/chvote-receiver/backend/vr-backend-mock-server/vr-backend-mock-server-rest
SPRING_PROFILES_ACTIVE=dev,with-local-pact mvn spring-boot:run
```

Build and run the Vote Receiver frontend:

```
cd $WORKSPACE_CHVOTE/chvote-receiver/frontend/
npm install
npm start
```

Now create some test data: open the browser at [http://localhost:57646/mock/swagger-ui.html](http://localhost:57646/mock/swagger-ui.html).
Go to the `database-controller` entry then `create-use-case` in order to create an operation.
Only the following parameters are supported for now:

- `canton`: drop-down list of cantons. 
- `dateType`: specify if it's a current, past or future operation
- `groupedVotation`: boolean to choose a display mode in case of multiple votations
- `type`: the operation type
- `withElection`: boolean that must be setted to `false` as it's not yet supported
- `withVotation`: boolean that should be setted to `true`. Otherwise you will have an empty operation 


Finally, go to the vote receiver frontend at [http://localhost:8080/test/](http://localhost:8080/test/). You should now
see the created operation.

## 10. Start the VRUM web application

Build the VRUM backend:

```
cd $WORKSPACE_CHVOTE/chvote-vrum/chvote-vrum-backend

MB_HOST=$DOCKER_HOSTNAME \
mvn clean install
```

Run the VRUM backend application:

```
cd $WORKSPACE_CHVOTE/chvote-vrum/chvote-vrum-backend/chvote-vrum-rest

MB_HOST=$DOCKER_HOSTNAME \
SPRING_PROFILES_ACTIVE=development \
mvn spring-boot:run
```

Run the VRUM mock server:

```
cd $WORKSPACE_CHVOTE/chvote-vrum/chvote-vrum-backend/chvote-vrum-mock-server/chvote-vrum-mock-server-rest
mvn spring-boot:run
```

Build and run the VRUM frontend:

```
cd $WORKSPACE_CHVOTE/chvote-vrum/chvote-vrum-frontend/
npm install
npm start
```

To test the user interface, go to [http://localhost:4500](http://localhost:4500) and log in with user `ge1` and 
password `pass`.

To generate some test data, go to the mock server UI at [http://localhost:48646/mock/swagger-ui.html](http://localhost:48646/mock/swagger-ui.html):
and run `POST /database`.

## 11. Start the U-Print offline application

Build the U-Print application:

```
cd $WORKSPACE_CHVOTE/u-print
mvn clean verify -P<platform>
```

where `<platform>` can be:

- `windows`
- `linux`
- `mac`

Run the application:

```
cd $WORKSPACE_CHVOTE/u-print
$JAVA_HOME/bin/java -jar u-print-gui/target/u-print.jar
```

## 12. Start the U-Count offline application

Build the U-Count application:

```
cd $WORKSPACE_CHVOTE/u-count
mvn clean verify -P<platform>
```

where `<platform>` can be:

- `windows`
- `linux`
- `mac`

Run the application:

```
cd $WORKSPACE_CHVOTE/u-count
$JAVA_HOME/bin/java -jar u-count-gui/target/u-count.jar
```

## 13. Run an end-to-end test

Use the [published test data]({{ site.chvote-root-url }}/documentation/chvote-docs-functional/tree/master/Test-data) to run
a complete end-to-end test of the CHVote platform.

Also watch the [demonstration video](/demo) to learn how to use the CHVote applications.

